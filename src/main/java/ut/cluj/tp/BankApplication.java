package ut.cluj.tp;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.scheduling.annotation.EnableScheduling;
import ut.cluj.tp.bankingsystem.account.CheckingAccount;
import ut.cluj.tp.bankingsystem.account.SavingAccount;
import ut.cluj.tp.bankingsystem.bank.Bank;
import ut.cluj.tp.bankingsystem.bank.BankProcess;
import ut.cluj.tp.bankingsystem.customers.Person;
import ut.cluj.tp.exceptions.NotEnoughMoneyException;
import ut.cluj.tp.exceptions.OverdrawnException;

@EnableScheduling
@EnableEncryptableProperties
@SpringBootApplication
public class BankApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}
}
