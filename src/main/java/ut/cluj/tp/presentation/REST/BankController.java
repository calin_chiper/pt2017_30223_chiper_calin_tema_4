package ut.cluj.tp.presentation.REST;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ut.cluj.tp.bankingsystem.account.Account;
import ut.cluj.tp.bankingsystem.account.CheckingAccount;
import ut.cluj.tp.bankingsystem.account.SavingAccount;
import ut.cluj.tp.bankingsystem.bank.Bank;
import ut.cluj.tp.bankingsystem.customers.Person;
import ut.cluj.tp.exceptions.NotEnoughMoneyException;
import ut.cluj.tp.exceptions.OverdrawnException;

import java.util.ArrayList;
import java.util.List;


@RestController
public class BankController {

    private final Bank bank;

    @Autowired
    public BankController(Bank bank) {
        this.bank = bank;
    }

    @RequestMapping(value = "/customer_form", method = RequestMethod.POST)
    public ResponseEntity<Person> addNewCustomer(@RequestBody Person person) {
        this.bank.addCustomer(person);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @RequestMapping(value = "/account_form", method = RequestMethod.POST)
    public ResponseEntity<BankAccount> createNewAccount(@RequestBody BankAccount bankAccount) {
        Account account = null;
        if (bankAccount.getType().equals("Saving")) {
            account = new SavingAccount(bankAccount.getIban(), bankAccount.getBalance());
        } else if (bankAccount.getType().equals("Checking")) {
            account = new CheckingAccount(bankAccount.getIban(), bankAccount.getBalance());
        }

        this.bank.createAccount(bankAccount.getPerson(), account);
        return new ResponseEntity<>(bankAccount, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public ResponseEntity<List<Person>> listAllCustomers() {
        return new ResponseEntity<>(this.bank.getAllCustomers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{personalIdentificationNumber}", method = RequestMethod.GET)
    public ResponseEntity<List<BankAccount>> listAccounts(@PathVariable Long personalIdentificationNumber) {
        List<BankAccount> personalBankAccounts = getBankAccounts(personalIdentificationNumber);
        return new ResponseEntity<>(personalBankAccounts, HttpStatus.OK);
    }

    private List<BankAccount> getBankAccounts(long personalIdentificationNumber) {
        List<Person> people = this.bank.getAllCustomers();
        List<BankAccount> personalBankAccounts = new ArrayList<>();
        String accountType = "";

        for(Person person: people) {
            if(person.getPersonalIdentificationNumber() == personalIdentificationNumber) {
                List<Account> accounts = this.bank.listPersonalAccounts(person);

                for(Account account: accounts) {
                    if(account instanceof SavingAccount) {
                        accountType = "Saving";
                    } else if (account instanceof CheckingAccount) {
                        accountType = "Checking";
                    }

                    personalBankAccounts.add(new BankAccount(account.getIBAN(),
                            account.getBalance(),
                            accountType,
                            person));
                }

            }
        }
        return personalBankAccounts;
    }

    @RequestMapping(value = "/accounts/{personalIdentificationNumber}/{iban}", method = RequestMethod.DELETE)
    public ResponseEntity<BankAccount> disableAccount(@PathVariable Long personalIdentificationNumber,
                                                      @PathVariable Long iban) {
        List<Person> people = this.bank.getAllCustomers();
        people.forEach(person -> {
            if(person.getPersonalIdentificationNumber() == personalIdentificationNumber) {
                List<Account> accounts = this.bank.listPersonalAccounts(person);
                accounts.forEach(account -> {
                    if(account.getInternationalBankAccountNumber() == iban) {
                        this.bank.disableAccount(person, account);
                    }
                });
            }
        });
        return new ResponseEntity<>(new BankAccount(), HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{personalIdentificationNumber}", method = RequestMethod.DELETE)
    public ResponseEntity<Person> removeCustomer(@PathVariable Long personalIdentificationNumber) {
        List<Person> people = this.bank.getAllCustomers();
        people.forEach(person -> {
            if(person.getPersonalIdentificationNumber() == personalIdentificationNumber) {
                this.bank.removeCustomer(person);
            }
        });
        return  new ResponseEntity<Person>(new Person(), HttpStatus.OK);
    }

    @RequestMapping(value ="/deposit/{amount}", method = RequestMethod.POST)
    public ResponseEntity<BankAccount> deposit(@PathVariable Integer amount, @RequestBody BankAccount bankAccount) {
        Account account;
        if(bankAccount.getType().equals("Saving")) {
            account = new SavingAccount(bankAccount.getIban(), bankAccount.getBalance());
        } else {
            account = new CheckingAccount(bankAccount.getIban(), bankAccount.getBalance());
        }
        this.bank.depositTo(bankAccount.getPerson(), account, amount);
        return new ResponseEntity<>(new BankAccount(), HttpStatus.OK);
    }

    @RequestMapping(value ="/withdraw/{amount}", method = RequestMethod.POST)
    public ResponseEntity<BankAccount> withdraw(@PathVariable Integer amount, @RequestBody BankAccount bankAccount) {
        Account account;
        if(bankAccount.getType().equals("Saving")) {
            account = new SavingAccount(bankAccount.getIban(), bankAccount.getBalance());
        } else {
            account = new CheckingAccount(bankAccount.getIban(), bankAccount.getBalance());
        }
        try {
            this.bank.withdrawFrom(bankAccount.getPerson(), account, amount);
        } catch (NotEnoughMoneyException e) {
            return new ResponseEntity<>(new BankAccount(-1, -404, "NotEnoughMoney", new Person()),
                    HttpStatus.BAD_REQUEST);
        } catch (OverdrawnException e) {
            return new ResponseEntity<>(new BankAccount(-1, -404, "OverdrawnLimitReached", new Person()),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BankAccount(), HttpStatus.OK);
    }
}

class BankAccount {
    private long iban;
    private double balance;
    private String type;
    private Person person;

    public BankAccount(long iban, double balance, String type, Person person) {
        this.iban = iban;
        this.balance = balance;
        this.type = type;
        this.person = person;
    }

    public BankAccount() {}

    public long getIban() {
        return iban;
    }

    public void setIban(long iban) {
        this.iban = iban;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return iban + " " + balance + " " + type + " " + person.getPersonalIdentificationNumber();
    }
}

