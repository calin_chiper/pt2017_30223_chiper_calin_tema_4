package ut.cluj.tp.presentation.REST;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ut.cluj.tp.bankingsystem.bank.Bank;
import ut.cluj.tp.bankingsystem.customers.Person;

import java.util.List;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    Bank bank;

    @RequestMapping(method = RequestMethod.GET)
    public List<Person> test() {
        return bank.getAllCustomers();
    }
}
