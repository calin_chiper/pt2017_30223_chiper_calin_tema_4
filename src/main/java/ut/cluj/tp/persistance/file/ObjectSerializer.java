package ut.cluj.tp.persistance.file;




import java.io.*;



public class ObjectSerializer {
    public void saveObject(String filePath, Object object) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(filePath + ".ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    public Object loadObject(String filePath) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(filePath + ".ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Object object = objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        return object;
    }
}
