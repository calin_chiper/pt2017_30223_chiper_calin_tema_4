package ut.cluj.tp.persistance.file;

import org.springframework.stereotype.Repository;
import ut.cluj.tp.bankingsystem.account.Account;
import ut.cluj.tp.bankingsystem.customers.Person;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Repository(value = "accountDTO")
public class AccountDTO {
    private ObjectSerializer objectSerializer;

    public AccountDTO() {
        this.objectSerializer = new ObjectSerializer();
    }

    public void createAccountDataFile(Map<Person, Set<Account>> accounts) {
        try {
            this.objectSerializer.saveObject("bank-data", accounts);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public Map<Person, Set<Account>> readAccountDataFile() {
        Map<Person, Set<Account>> accounts = new HashMap<>();
        try {
             accounts = (Map<Person, Set<Account>>) this.objectSerializer.loadObject("bank-data");
        } catch (IOException | ClassNotFoundException e) {
            this.createAccountDataFile(accounts);
        }
        return accounts;
    }

}
