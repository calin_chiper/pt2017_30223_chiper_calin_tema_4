package ut.cluj.tp.exceptions;


public class OverdrawnException extends Exception {
    public OverdrawnException() {
        super("Overdrawn limit reached");
    }
}
