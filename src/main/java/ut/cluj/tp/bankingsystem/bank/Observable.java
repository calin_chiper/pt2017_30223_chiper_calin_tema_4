package ut.cluj.tp.bankingsystem.bank;


import ut.cluj.tp.bankingsystem.contact.email.Observer;

public interface Observable {

    void registerObserver(Observer observer);

    void notifyObserver(Observer observer, Object ...args);
}
