package ut.cluj.tp.bankingsystem.bank;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ut.cluj.tp.bankingsystem.account.Account;
import ut.cluj.tp.bankingsystem.account.SavingAccount;
import ut.cluj.tp.bankingsystem.contact.email.*;
import ut.cluj.tp.bankingsystem.contact.email.Observer;
import ut.cluj.tp.bankingsystem.customers.Person;
import ut.cluj.tp.exceptions.NotEnoughMoneyException;
import ut.cluj.tp.exceptions.OverdrawnException;
import ut.cluj.tp.persistance.file.AccountDTO;


import java.util.*;

@Component(value = "bank")
public class Bank implements BankProcess<Person>, Observable  {

    private Map<Person, Set<Account>> accounts;
    private int currentTransferFee;
    private final AccountDTO accountDTO;
    private Observer customerEmailSupport;

    @Autowired
    public Bank(AccountDTO accountDTO, Observer customerEmailSupport) {
        this.accounts = new HashMap<>();
        this.accountDTO = accountDTO;
        this.currentTransferFee = 5;
        this.accounts = this.accountDTO.readAccountDataFile();
        this.customerEmailSupport = customerEmailSupport;
    }

    public void setCurrentTransferFee(int currentTransferFee) {
        this.currentTransferFee = currentTransferFee;
        for(Person customer: this.accounts.keySet()) {
            this.notifyObserver(this.customerEmailSupport, customer,
                    String.format(MessageTemplates.FEE_CHANGED_MESSAGE, currentTransferFee));
        }
    }

    @Scheduled(fixedRate = 30000)
    public void resetOverdrawnLimit() {
        System.out.println("After 30 days: Overdrawn limit has been reset");
        for(Person customer: this.accounts.keySet()) {
            for(Account account: this.listPersonalAccounts(customer)) {
                if(account instanceof SavingAccount) {
                    ((SavingAccount) account).resetOverdrawnLimit();
                }
            }
        }
    }

    /*Customer management*/
    @Override
    public void addCustomer(Person customer) {
        int oldNumberOfCustomers = this.getNumberOfCustomers();
        assert customer != null;

        this.accounts.put(customer, new HashSet<>());
        this.accountDTO.createAccountDataFile(this.accounts);

        assert oldNumberOfCustomers + 1 == this.getNumberOfCustomers(); //can be caused by duplicates
        assert isWellFormed();
    }

    @Override
    public void removeCustomer(Person customer) {
        int oldNumberOfCustomers = this.getNumberOfCustomers();
        assert this.getNumberOfCustomers() > 0;
        assert customer != null;

        this.accounts.remove(customer);
        this.accountDTO.createAccountDataFile(this.accounts);

        assert oldNumberOfCustomers - 1 == this.getNumberOfCustomers();
        assert isWellFormed();
    }

    @Override
    public int getNumberOfCustomers() {
        return this.accounts.size();
    }

    @Override
    public List<Person> getAllCustomers() {
        List<Person> personList = new ArrayList<>();
        personList.addAll(this.accounts.keySet());
        return personList;
    }

    /*Account management*/
    @Override
    public void createAccount(Person customer, Account account) {
        assert customer != null && account != null;
        int oldNumberOfAccounts = this.getNumberOfAccounts();

        this.accounts.get(customer).add(account);
        this.accountDTO.createAccountDataFile(this.accounts);
        assert oldNumberOfAccounts + 1 == this.getNumberOfAccounts();
        assert isWellFormed();
    }

    @Override
    public void disableAccount(Person customer, Account account) {
        assert this.getNumberOfAccounts() > 0;
        assert account != null;
        assert customer != null;
        int oldNumberOfAccounts = this.getNumberOfAccounts();

        this.accounts.get(customer).remove(account);
        this.accountDTO.createAccountDataFile(this.accounts);

        assert oldNumberOfAccounts - 1 == this.getNumberOfAccounts();

        assert isWellFormed();
    }

    @Override
    public int getNumberOfAccounts() {
        int numberOfAccounts = 0;
        for(Map.Entry<Person, Set<Account>> entrySet: this.accounts.entrySet()) {
            numberOfAccounts += entrySet.getValue().size();
        }
        return numberOfAccounts;
    }

    @Override
    public List<Account> listPersonalAccounts(Person customer) {
        assert customer != null;

        List<Account> accountList = new ArrayList<>();
        accountList.addAll(this.accounts.get(customer));
        return accountList;
    }

    /*Class Invariant*/
    @Override
    public boolean isWellFormed() {
        /*Check if the accounts map is null*/
        if(this.accounts == null) return false;

        /*Check if contains null values or null keys*/
        int numberOfCustomers = 0;
        for(Map.Entry<Person, Set<Account>> entrySet: this.accounts.entrySet()) {
            numberOfCustomers++;
            if(entrySet.getKey() == null || entrySet.getValue() == null) return false;
        }

        /*Check if the number of keys is equal to the number of customers*/
        return numberOfCustomers == this.accounts.keySet().size();
    }

    /*Bank transaction*/
    @Override
    public void withdrawFrom(Person customer, Account account, int amount) throws NotEnoughMoneyException, OverdrawnException {
        assert account != null && amount > 0;
        assert customer != null;
        double currentBalance = account.getBalance();
        double updatedBalance = 0;

        for(Account personalAccount: this.accounts.get(customer)) {
            if(personalAccount.equals(account)) {
                personalAccount.withdraw(amount, this.currentTransferFee);
                updatedBalance = personalAccount.getBalance();
                this.notifyObserver(this.customerEmailSupport, customer,
                        String.format(MessageTemplates.WITHDRAW_MESSAGE,
                                amount,
                                this.currentTransferFee,
                                updatedBalance));
                this.accountDTO.createAccountDataFile(this.accounts);
                break;
            }
        }

        assert currentBalance != updatedBalance;
    }

    @Override
    public void depositTo(Person customer, Account account, int amount) {
        assert account != null && amount > 0;
        assert customer != null;
        double currentBalance = account.getBalance();
        double updatedBalance = 0;

        for(Account personalAccount: this.accounts.get(customer)) {
            if(personalAccount.equals(account)) {
                personalAccount.deposit(amount, this.currentTransferFee);
                updatedBalance = personalAccount.getBalance();
                this.notifyObserver(this.customerEmailSupport, customer,
                        String.format(MessageTemplates.DEPOSIT_MESSAGE,
                                amount,
                                this.currentTransferFee,
                                updatedBalance));
                this.accountDTO.createAccountDataFile(this.accounts);
                break;
            }
        }

        assert currentBalance != updatedBalance;
    }

    @Override
    public final void registerObserver(Observer observer) {
        this.customerEmailSupport = customerEmailSupport;
    }

    @Override
    public void notifyObserver(Observer observer, Object ...arg) {
        this.customerEmailSupport.update(arg[0], arg[1]);
    }

    private class MessageTemplates {
         private final static String WITHDRAW_MESSAGE = "You have withdrawn $%d\nTransaction Fee: %d%\nNew balance: $%f";
         private final static String DEPOSIT_MESSAGE = "You have deposited $%d\nTransaction Fee: %d%\nNew balance: $%f";
         private final static String FEE_CHANGED_MESSAGE = "The transaction fee has been changed to $%d";
    }
}
