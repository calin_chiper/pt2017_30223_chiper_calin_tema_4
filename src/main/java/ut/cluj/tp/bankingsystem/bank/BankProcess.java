package ut.cluj.tp.bankingsystem.bank;


import ut.cluj.tp.bankingsystem.account.Account;
import ut.cluj.tp.exceptions.NotEnoughMoneyException;
import ut.cluj.tp.exceptions.OverdrawnException;

import java.util.List;

/**
 * @invariant isWellFormed()
 */
public interface BankProcess<T> {
    /**
     *@pre customer != null
     *@post getNumberOfCustomers() == getNumberOfCustomers()@pre + 1
     */
    void addCustomer(T customer);

    /**
     * @pre getNumberOfCustomers() > 0
     * @pre  customer != null
     * @post getNumberOfCustomers() == getNumberOfCustomers()@pre - 1
     *
     */
    void removeCustomer(T customer);

    /**
     *@pre customer != null && account != null
     *@post getNumberOfAccounts() == getNumberOfAccounts()@pre + 1
     */
    void createAccount(T customer, Account account);

    /**
     * @pre getNumberOfAccounts() > 0
     * @pre  account != null
     * @pre customer != null
     * @post getNumberOfAccounts() == getNumberOfAccounts()@pre - 1
     */
    void disableAccount(T customer, Account account);

    boolean isWellFormed();

    /**
     *@pre account != null && amount > 0
     *@pre customer != null
     *@post account.getBalance()@pre != account.getBalance()
     */
    void withdrawFrom(T customer, Account account, int amount) throws NotEnoughMoneyException, OverdrawnException;

    /**
     *@pre account != null && amount > 0
     *@pre customer != null
     *@post account.getBalance()@pre != account.getBalance()
     */
    void depositTo(T customer, Account account, int amount);

    /**
     * @pre true
     * @post @nochange
     */
    int getNumberOfCustomers();

    /**
     * @pre true
     * @post @nochange
     */
    int getNumberOfAccounts();

    /**
     * @pre true
     * @post @nochange
     */
    List<T> getAllCustomers();

    /**
     * @pre customer != null
     * @post @nochange
     */
    List<Account> listPersonalAccounts(T customer);

}
