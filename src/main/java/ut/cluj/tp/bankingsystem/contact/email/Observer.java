package ut.cluj.tp.bankingsystem.contact.email;


import ut.cluj.tp.bankingsystem.bank.Observable;

public interface Observer {

    void update(Object ...arg);

    void subscribeTo(Observable observable);
}
