package ut.cluj.tp.bankingsystem.contact.email;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ut.cluj.tp.bankingsystem.bank.Observable;
import ut.cluj.tp.bankingsystem.customers.Person;

@Service(value = "customerEmailSupport")
public class CustomerEmailSupport implements EmailService, Observer{

    private final JavaMailSender emailSender;
    private Observable observable;

    @Autowired
    public CustomerEmailSupport(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    @Override
    public void update(Object ...arg) throws UnsupportedOperationException {
        if(arg.length > 2) {
            throw new UnsupportedOperationException();
        }
        Person customer = (Person) arg[0];
        this.sendSimpleMessage(customer.getEmail(), "Bank transaction", String.valueOf(arg[1]));
    }

    @Override
    public void subscribeTo(Observable observable) {
        this.observable = observable;
    }
}
