package ut.cluj.tp.bankingsystem.contact.email;


public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
