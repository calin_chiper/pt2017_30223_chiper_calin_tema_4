package ut.cluj.tp.bankingsystem.account;


import ut.cluj.tp.exceptions.NotEnoughMoneyException;

public class CheckingAccount extends Account {

    public CheckingAccount(long internationalBankAccountNumber, double initialDeposit) {
        super(internationalBankAccountNumber, initialDeposit);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        } else if (!(object instanceof CheckingAccount)) {
            return false;
        }
        Account account = (Account) object;
        if (account.getBalance() != this.getBalance()) {
            return false;
        } else if (account.getInternationalBankAccountNumber() != this.getInternationalBankAccountNumber()) {
            return false;
        }
        return true;
    }

    @Override
    public void deposit(double amount, double transactionFee) {
        this.setBalance(this.getBalance() + amount);
        extractTransactionFee(transactionFee);
    }

    @Override
    public void withdraw(double amount, double transactionFee) throws NotEnoughMoneyException {
        if ((this.getBalance() - amount) - this.getBalance() * (transactionFee / 100) < 0) {
            throw new NotEnoughMoneyException("Invalid transaction");
        }
        this.setBalance(this.getBalance() - amount);
        extractTransactionFee(transactionFee);
    }


}
