package ut.cluj.tp.bankingsystem.account;



import ut.cluj.tp.exceptions.NotEnoughMoneyException;
import ut.cluj.tp.exceptions.OverdrawnException;

public class SavingAccount extends Account {

    private static final int LOW_INTEREST_RATE = 2;
    private static final int HIGH_INTEREST_RATE = 5;

    private int overdrawnLimit; // it resets after a month

    public SavingAccount(long internationalBankAccountNumber, double initialDeposit) {
        super(internationalBankAccountNumber, initialDeposit);
        this.overdrawnLimit = 3;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        } else if (!(object instanceof SavingAccount)) {
            return false;
        }
        Account account = (Account) object;
        if (account.getBalance() != this.getBalance()) {
            return false;
        } else if (account.getInternationalBankAccountNumber() != this.getInternationalBankAccountNumber()) {
            return false;
        }
        return true;
    }


    public void resetOverdrawnLimit() {
        this.overdrawnLimit = 3;
    }

    @Override
    public void deposit(double amount, double transactionFee) {
        if (amount < 10000) {
            this.setBalance(this.getBalance() + amount + (amount * LOW_INTEREST_RATE / 100));
        } else {
            this.setBalance(this.getBalance() + amount + (amount * HIGH_INTEREST_RATE / 100));
        }
        extractTransactionFee(transactionFee);
    }

    @Override
    public void withdraw(double amount, double transactionFee) throws OverdrawnException, NotEnoughMoneyException {
        if (overdrawnLimit == 0) {
            throw new OverdrawnException();
        } else if ((this.getBalance() - amount) - this.getBalance() * (transactionFee / 100) < 0) {
            throw new NotEnoughMoneyException("Invalid transaction");
        }
        this.setBalance(this.getBalance() - amount);
        this.extractTransactionFee(transactionFee);
        this.overdrawnLimit--;
    }
}
