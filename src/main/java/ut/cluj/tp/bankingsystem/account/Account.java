package ut.cluj.tp.bankingsystem.account;



import ut.cluj.tp.exceptions.NotEnoughMoneyException;
import ut.cluj.tp.exceptions.OverdrawnException;

import java.io.Serializable;

public abstract class Account implements Serializable {
    private long IBAN;
    private double balance;

    protected Account(long internationalBankAccountNumber, double initialDeposit) {
        this.IBAN = internationalBankAccountNumber;
        this.balance = initialDeposit;
    }

    public double getBalance() {
        return this.balance;
    }

    public long getIBAN() {
        return this.IBAN;
    }

    public void setIBAN(long IBAN) {
        this.IBAN = IBAN;
    }

    protected void setBalance(double balance) throws UnsupportedOperationException {
        if(balance > 0) {
            this.balance = balance;
        }
    }

    public long getInternationalBankAccountNumber() {
        return this.IBAN;
    }


    protected void extractTransactionFee(double transactionFee) {
        this.setBalance(this.getBalance() - this.getBalance() * (transactionFee / 100));
    }

    @Override
    public int hashCode() {
        int result = (int) (this.IBAN ^ (this.IBAN >>> 32));
        result = 31 * result + (int) (Double.doubleToLongBits(this.balance));
        return result;
    }

    @Override
    public abstract boolean equals(Object o);

    public abstract void withdraw(double amount, double transactionFee) throws NotEnoughMoneyException, OverdrawnException;

    public abstract void deposit(double amount, double transactionFee);


}
