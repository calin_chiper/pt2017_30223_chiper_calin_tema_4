package ut.cluj.tp.bankingsystem.customers;


import java.io.Serializable;


public class Person implements Serializable {
    private long personalIdentificationNumber;
    private String firstName;
    private String lastName;
    private String email;

    public Person(long personalIdentificationNumber, String firstName, String lastName, String email) {
        this.personalIdentificationNumber = personalIdentificationNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Person() {}


    @Override
    public int hashCode() {
        int result = (int) Double.doubleToLongBits(this.personalIdentificationNumber);
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        } else if (!(object instanceof Person)) {
            return false;
        }
        Person person = (Person) object;
        if (person.getPersonalIdentificationNumber() != this.getPersonalIdentificationNumber()) {
            return false;
        } else if (!person.getFirstName().equals(this.getFirstName())) {
            return false;
        } else if (!person.getLastName().equals(this.getLastName())) {
            return false;
        } else if (!person.getEmail().equals(this.getEmail())) {
            return false;
        }
        return true;
    }

    public long getPersonalIdentificationNumber() {
        return personalIdentificationNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setPersonalIdentificationNumber(long personalIdentificationNumber) {
        this.personalIdentificationNumber = personalIdentificationNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
