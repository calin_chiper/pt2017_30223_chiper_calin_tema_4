package ut.cluj.tp.bankingsystem.account;

import org.junit.Test;

import static org.junit.Assert.*;


public class AccountTest {
    private Account savingAccount;
    private Account checkingAccount;

    public AccountTest() {
        this.savingAccount = new SavingAccount(123, 100);
        this.checkingAccount = new CheckingAccount(123, 100);
    }

    @Test
    public void hashCodeTest() throws Exception {
        assertNotEquals(savingAccount.hashCode(), checkingAccount);
    }

    @Test
    public void equalsTest() throws Exception {
        assertNotEquals(savingAccount, checkingAccount);
    }

    @Test
    public void withdrawTest() throws Exception {
        this.savingAccount = new SavingAccount(123, 100);
        this.checkingAccount = new CheckingAccount(456, 100);

        this.savingAccount.withdraw(5, 5);
        this.checkingAccount.withdraw(5, 5);

        assertEquals(this.savingAccount.getBalance(), this.checkingAccount.getBalance(), 0.1);
    }

    @Test
    public void depositTest() throws Exception {
        this.savingAccount = new SavingAccount(123, 100);
        this.checkingAccount = new CheckingAccount(456, 100);

        double oldCheckingBalance = checkingAccount.getBalance();
        double oldSavingBalance = savingAccount.getBalance();

        this.savingAccount.deposit(5, 15);
        this.checkingAccount.deposit(5, 15);

        assertNotEquals(this.savingAccount.getBalance(), this.checkingAccount.getBalance());
        assertEquals(this.savingAccount.getBalance(), 89.335, 0.1);
        assertEquals(this.checkingAccount.getBalance(), 89.25, 0.1);
    }

}